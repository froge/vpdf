module main

/*
struct Stream {
	content string
}

type Dictionary = map[string]Object
*/
type Object = Dictionary | Stream | bool | f32 | int | string

struct Dictionary {
	items map[string]Object
}

fn (dict Dictionary) str() string {
	mut temp := '<< '
	for k, v in dict.items {
		temp += '$k $v'
	}
	return temp
}

enum RenderMode {
	fill
	stroke
	fillstroke
	inv
	fillpath
	strokepath
	fillstrokepath
	path
}

struct Stream {
mut:
	content string
}

struct Page {
	contents Stream
}

struct Document {
mut:
	cur_object int
	objects    []int
	text       string
	xref_pos   int
	pages      []Page
}

fn main() {
	mut doc := Document{}
	mut text := &Stream{}
	text.begin_text()
	text.set_font('F13', 12)
	text.move(400, 700)
	text.add_text('test')
	text.set_font('F13', 12)
	text.set_render(.stroke)
	text.set_rise(-5)
	text.add_text('lol')
	text.end_text()
	doc.pages << Page{
		contents: text
	}
	doc.finish()
	println(doc.text)
}

fn (mut doc Document) add_stream(content string) {
	doc.add_object('<< /Length $content.len >>\nstream\n${content}endstream')
}

// coordinates from the bottom left, in 1/72 inches
fn (mut doc Document) add_text(text string, x int, y int) {
	doc.add_stream('BT\n/F13 12 Tf\n$x $y Td\n($text) Tj\nET')
}

fn (mut doc Document) add_header() {
	doc.text += '%PDF-1.7\n'
	doc.cur_object = 1
}

fn (mut doc Document) add_pages() {
	mut page_objs := ''
	mut page_num := doc.cur_object
	for _ in doc.pages {
		page_num++
		page_objs += '\n$page_num 0 R'
		page_num++
	}
	doc.add_object('<</Type /Pages\n/MediaBox [ 0 0 595 842 ]\n/Kids [$page_objs\n]\n/Count $doc.pages.len\n>>')
	page_num = doc.cur_object
	for page in doc.pages {
		doc.add_object('<</Type /Page\n/Parent 2 0 R\n/Contents ${doc.cur_object + 1} 0 R>>')
		doc.add_stream(page.contents.content)
	}
}

fn (mut doc Document) add_object(obj string) {
	doc.objects << doc.text.len - 1
	doc.text += '$doc.cur_object 0 obj\n$obj\nendobj\n'
	doc.cur_object++
}

fn (mut doc Document) add_xref() {
	mut xrefs := ''
	for obj in doc.objects {
		xrefs += '${obj:010} 00000 n\n'
	}
	doc.xref_pos = doc.text.len
	doc.text += 'xref\n0 $doc.cur_object\n0000000000 65535 f\n$xrefs'
}

fn (mut doc Document) add_trailer() {
	doc.text += 'trailer\n<<\n/Root 1 0 R\n/Size $doc.cur_object\n>>\nstartxref\n$doc.xref_pos\n%%EOF'
}

fn (mut doc Document) finish() {
	doc.add_header()
	doc.add_object('<</Type /Catalog\n/Pages 2 0 R\n>>')
	doc.add_pages()
	doc.add_xref()
	doc.add_trailer()
}

fn (mut stream Stream) begin_text() {
	stream.content += 'BT\n'
}

fn (mut stream Stream) set_font(font string, pt_size int) {
	stream.content += '/$font $pt_size Tf\n'
}

fn (mut stream Stream) move(x int, y int) {
	stream.content += '$x $y Td\n'
}

fn (mut stream Stream) break_line() {
	stream.content += 'T*\n'
}

fn (mut stream Stream) add_text(text string) {
	stream.content += '($text) Tj\n'
}

fn (mut stream Stream) set_render(mode RenderMode) {
	stream.content += '${int(mode)} Tr\n'
}

fn (mut stream Stream) set_rise(rise int) {
	stream.content += '$rise Ts'
}

fn (mut stream Stream) end_text() {
	stream.content += 'ET\n'
}
